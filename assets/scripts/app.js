// Uncomment the next line if you want to use bootstrap, don't forget uncomment jQuery defination in webpack.common.js line 93
import 'bootstrap';

$(function() {
	$("#ShowMenu").click(function(event) {
		$("#menu").css({
			"display": 'block',
			"top": '0px'
		});
	});
});

$(function() {
	$("#closeMenu").click(function(event) {
		$("#menu").css({
			"display": 'none',
			"top": '0px'
		});
	});
});