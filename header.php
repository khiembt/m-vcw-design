<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Vicoders HTML/CSS/JS Development Kit</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="dist/app.css" />
    <script src="dist/app.js"></script>
</head>

<body>
    <div id="header" class="position-relative">
        <!-- <div id="menu_header" class="float-right">
				
					 <p>a</p>
					<form>
				 <div class="form-group">
			    		<input type="text" class="form-control d-sm-inline" id="text_search" placeholder="">
			    		<span><i class="fa fa-search icon_menu" aria-hidden="true"></i></span>
			 	 </div> 	
			
		</div> -->
        <!-- <div class="container bg-dark" >
			<div class="row">
			<div class="col-lg-6">
				<div class="logo_header">
				<img src="assets/images/logo.png" src="" alt="">
				</div>
			</div>
			<div class="col-lg-6 ">
				<span class="search float-right">
				<i class="fa fa-search" aria-hidden="true"></i>
				<i class="fa fa-bars" aria-hidden="true"></i>
				</span>
			</div>
			</div>
		</div> -->
        <!-- <div class="logo">
					<img src="assets/images/logo.png" src="" alt="">
				</div>
				<div class="menu-bar">
					<div class="search">
						<i class="fa fa-search" aria-hidden="true"></i>
					</div>
				</div> -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <div class="logo">
                        <img src="assets/images/logo.png" src="" alt="">
						</div>
                    </div>
                    <div class="col-lg-6 ">
                        <span class="icon_menu float-right">
							<i class="fa fa-search" aria-hidden="true"></i>
							<i class="fa fa-bars"  aria-hidden="true" id="ShowMenu"></i>
						</span>
                    </div>
                </div>

            </div>
        </div>
            <div id="menu" class="position-absolute">
                    <div class="form_menu">
                        <input type="text" name="" placeholder="|">
                        <i class="fa fa-search" aria-hidden="true"></i>
                        <i class="fa fa-times" aria-hidden="true" id="closeMenu"></i>
                    </div>
                    <div class="list_menu ">
                    	<ul class="list-group">
                    		<li class="list-group-item"><a href="">Về chúng tôi</a></li>
                    	</ul>
                    	<ul class="list-group">
                    		<li class="list-group-item"><a href="">Dự án</a></li>
                    	</ul>
                    	<ul class="list-group">
                    		<li class="list-group-item"><a href="">Tin tức</a></li>
                    	</ul>
                    	<ul class="list-group">
                    		<li class="list-group-item"><a href="">Liên hệ <i class="fa fa-chevron-right ml-5" aria-hidden="true"></i></a></li>
                    	</ul>
                    </div>
                    <div class="lang_menu">
                    	<span>VN</span>
                    	<span>EN</span>
                    </div>
                </div>